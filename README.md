Evaluation Datasets for Cross-lingual Semantic Textual Similarity
--------------------

This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/

This package contains the English test sets for the 2017 Semantic Textual Similarity (STS) shared task track 5.
Each file has one sentence per line. The STS evaluation pair consists of the first and second part (sentence) each stored in a separate file.

To get the evaluation data for e.g. EN-CS load the file STS.2017.input.track5.EN.first.txt and STS.2017.input.track5.CS.second.txt and the gold standard in file STS.2017.gs.track5.first-second.txt. The lines in all files correspond with each other.
The input files are provided in UTF-8.

Citation
Please, cite our article if you use any of the available resources.
TBD

Input Files (UTF-8):
--------------------

- STS.2017.input.track5.FR.second.txt
- STS.2017.input.track5.FR.first.txt
- STS.2017.input.track5.EN.second.txt
- STS.2017.input.track5.EN.first.txt
- STS.2017.input.track5.DE.second.txt
- STS.2017.input.track5.DE.first.txt
- STS.2017.input.track5.CS.second.txt
- STS.2017.input.track5.CS.first.txt
- STS.2017.gs.track5.first-second.txt


